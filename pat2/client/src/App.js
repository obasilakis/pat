import React, { Component } from 'react';
import './App.css';
import { DrizzleContext } from 'drizzle-react';
import ListToken from "./components/ListToken/ListToken";
import LaunchToken from "./components/LaunchToken/LaunchToken";


const App = () =>  (
      <DrizzleContext.Consumer>
        {drizzleContext => {
          const { drizzle, drizzleState, initialized } = drizzleContext;

          if (!initialized) {
            return "Loading...";
          }

          return (
            <div className="App">
              <LaunchToken
                drizzle={drizzle}
                drizzleState={drizzleState}
              />
              <hr />
              <ListToken
                drizzle={drizzle}
                drizzleState={drizzleState} />
            </div>
          );
        }}
      </DrizzleContext.Consumer>
    )
    // WITHOUT CONTEXT
    //   if (this.state.loading) return "Loading Drizzle...";
    //   return <div className="App">
    //     <LaunchToken
    //       drizzle={this.props.drizzle}
    //       drizzleState={this.state.drizzleState}
    //     />
    //     <hr/>
    //     <ListToken
    //       drizzle={this.props.drizzle}
    //       drizzleState={this.state.drizzleState}
    //     />

    //   </div>;
    // }

export default App;
