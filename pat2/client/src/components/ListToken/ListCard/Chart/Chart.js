import React, {Component} from 'react';
const Recharts = require('recharts');

const {
  Area,
  // Line,
  XAxis,
  YAxis,
  CartesianGrid,
  Tooltip,
  // Legend,
  ReferenceDot,
  ComposedChart,
  Label
} = Recharts;

class CurveChart extends Component {

  state = {
    data: []
  }

  componentDidUpdate(prevProps) {
    if (prevProps != this.props) {
      let data = [];
      data = this.getChartData();
      this.setState({ data: data })
    }
  }

  getChartData() {
    let { totalSupply, poolBalance, invSlope, exponent } = this.props.curveData;
    poolBalance = parseFloat(poolBalance) || 0;
    totalSupply = parseFloat(totalSupply) || 0;

    let data = [];
    let step = (totalSupply || 50) / 100;

    // @TO DO: make scale of graph constant / stepwise, so you can more easily compare, e.g. by setting max
    // alternatively set YAxis prop: domain={[0, 20000]}
    let max = (totalSupply<1000) ? 1000 : ((totalSupply<10000) ? 10000 : totalSupply * 1.5 )

    for (let i = step; i < (totalSupply || 50) * 1.5 ; i += step) { // or use max here
      let price = 1 / invSlope * (i ** exponent);
      if (i < totalSupply) {
        data.push({ supply: i, sell: price.toFixed(4), value: parseFloat(price.toFixed(4)) });
      } else if (i >= totalSupply) {
        data.push({ supply: i, buy: price.toFixed(4), value: parseFloat(price.toFixed(4)) });
      }
    }
    console.log(data)
    return { data };
  }

  render() {
    console.log(this.props)
    let data = this.state.data

    return (
      <div >

        <ComposedChart
          style={{ margin: 'auto' }}
          width={this.props.width}
          height={this.props.height}
          data={data}
          margin={this.props.margin}
        >
          <CartesianGrid strokeDasharray="3 3" />
          <XAxis dataKey="supply" type={'number'} >
            <Label value="Token Supply" position="insideBottomRight" dy={20} />
          </XAxis>
          <YAxis dataKey="value" type={'number'}>
            <Label value="Token Price" position="insideTopLeft" style={{ textAnchor: 'right' }} angle={270} dy={100} offset={-20} />
          </YAxis>
          <Tooltip />

          <Area isAnimationActive={false} dots={false} stackOffset={'none'} dataKey="value" name={'price'} key={'price'} stroke='#0095b3' fill='none' />

          <Area isAnimationActive={false} stackOffset={'none'} dataKey="sell" stroke="#0095b3" fill='#0095b3' />

          <ReferenceDot
            isFront={true}
            ifOverflow="extendDomain"
            x={parseFloat(this.props.curveData.totalSupply)}
            y={parseFloat(this.props.curveData.currentPrice)}
            r={17}
            label={parseFloat(this.props.curveData.currentPrice.toFixed(2))}
          />

        </ComposedChart>


      </div>
    )
  }
}

export default CurveChart;