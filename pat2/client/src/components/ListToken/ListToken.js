import React, { Component } from "react";
import ListCard from './ListCard/ListCard';

class ListToken extends Component {

    state = {
        dataKey: null,
        instantiations: []
    };

    componentDidMount() {
        const { drizzle } = this.props;
        const contract = drizzle.contracts.MyTokenFactory;
        //@dev getInstantiationCount takes the Factory owner as an argument. 
        // this should be made dynamic in the future, to allow other systems to run the code without having to change this to their own test address.
        const dataKey = contract.methods["getInstantiations"].cacheCall()
        this.setState({ dataKey });
    }


    render() {
        const { MyTokenFactory } = this.props.drizzleState.contracts;
        const instantiations = MyTokenFactory.getInstantiations[this.state.dataKey];
        const text=JSON.stringify(instantiations);
        let inst = null;
        if (text) {
            inst = JSON.parse(text).value
        };
        const contracts = inst && inst.map(address=>{
            // drizzle.addContract(EthPolynomialCurvedToken, {
            //   @dev: Ideally the newly deployed token contract gets automatically dynamically added to drizzle store
            //   this might not be the right place to do that.
            //   syntax not clear from drizzle documentation!
            //   });

            return (
                <ListCard 
                    address={address}
                    key={address}
                    account={this.props.drizzleState.accounts[0]}
                    drizzle={this.props.drizzle}
                    drizzleState={this.props.drizzleState}
                />
            )
        })

        return (
            <div>
                {contracts}
            </div>
        )
    }
}

export default ListToken;