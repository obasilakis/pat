import React, { Component } from 'react'
import {
    Alert,
    Card,
    CardBody,
    CardTitle,
    CardText,
    CardImg,
    CardFooter,
    Form,
    FormGroup,
    Input,
    Button
} from 'reactstrap';
import EthPolynomialCurvedToken from '../../../contracts/EthPolynomialCurvedToken.json';
import ContractInfo from './ContractInfo/ContractInfo';
import BuySell from './BuySell/BuySell';


//const multiplier = 10 ** 18;


class ListCard extends Component {

    state = {
        tokenContract: null,
        tokenPurchaseAmount: null,
        price: null,
        reward: null
    }

    
    componentDidMount = async () => {
        const { drizzle, drizzleState, address } = this.props;
        const contractConfig = {
            contractName: address,
            web3Contract: new drizzle.web3.eth.Contract(
                EthPolynomialCurvedToken['abi'],
                address
            )
          };
          //TODO: should listen to Minted, Burned events and update component when they get fired
          let events = ['Minted', 'Burned'];
          await drizzle.addContract(contractConfig, events);
          const contract = drizzle.contracts[address];
          this.setState({drizzleContract: contract})
        
        // WITHOUT DRIZZLE / USING PLAIN WEB3
        // try {
        
        // var web3 = await new Web3(new Web3.providers.HttpProvider('http://localhost:8545'));
        // const contract_instance = await new web3.eth.Contract(abi, this.props.address);
        // this.setState({tokenContract: contract_instance});
        // } catch (error) {
        //     // Catch any errors for any of the above operations.
        //     alert(
        //       `Failed to load web3, accounts, or contract. Check console for details.`
        //     );
        //     console.log(error);
        //   }
    }

    render() {
        
        return (
            <Card >
                <CardBody>
                    <CardTitle>
                        X's Attention is on the market!
                    </CardTitle>
                    <ContractInfo contract={this.state.drizzleContract} drizzleState={this.props.drizzleState} address={this.props.address} account={this.props.account}/>
                    <CardText>
                        some info
                    </CardText>
                    <BuySell contract={this.state.drizzleContract} address={this.props.address} account={this.props.account}/>
                </CardBody>
            </Card>
        )
    }
}

export default ListCard;
